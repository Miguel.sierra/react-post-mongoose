import axios from "axios";
import Swal from "sweetalert2";

async function createSession(email, password) {
  try {
    let res = await axios({
      method: "POST",
      url: `${process.env.REACT_APP_BASE_URL_API}/login`,
      data: { email, password }
    });
    let { data } = res;
    return data.token;
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Wrong credentials, please check.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

async function destroySession(token) {
  try {
    await axios({
      method: "POST",
      url: `${process.env.REACT_APP_BASE_URL_API}/logout`,
      headers: { Authorization: `Bearer ${token}` }
    });
    return true;
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Fatal error.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

export { createSession, destroySession };
