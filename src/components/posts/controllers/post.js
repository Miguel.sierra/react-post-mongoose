import axios from "axios";
import Swal from "sweetalert2";

async function createPost(title, image, post, createdAt, token) {
  try {
    let formData = new FormData();
    formData.set("title", title);
    console.log(image);
    formData.set("image", image);
    formData.set("post", post);
    formData.set("createdAt", createdAt);

    let res = await axios({
      method: "POST",
      data: formData,
      url: `${process.env.REACT_APP_BASE_URL_API}/post`,
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data"
      }
    });
    let { data } = res;
    if (data) {
      Swal.fire({
        title: "Success!",
        text: "Post created",
        icon: "success",
        confirmButtonText: "Ok"
      });
    }
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Wrong data, please check.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

async function filterPosts(query, page, token) {
  try {
    let res = await axios({
      method: "GET",
      url: `${process.env.REACT_APP_BASE_URL_API}/posts/${query}?page=${page}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    let { data } = res;
    return data;
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Wrong data, please check.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

export { createPost, filterPosts };
