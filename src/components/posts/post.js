import React, { useState } from "react";
import { useCookies, Cookies } from "react-cookie";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment,
  Container,
  TextArea,
  Image,
  Divider,
  Pagination,
  Icon
} from "semantic-ui-react";
import { createPost } from "../posts/controllers/post";
import { filterPosts } from "../posts/controllers/post";

const PostFiltered = props => {
  const [cookies, setCookie, removeCookie] = useCookies(["session"]);
  let search = window.location.search;
  let params = new URLSearchParams(search);
  let query = params.get("query");
  let page = params.get("page");
  console.log(props);
  const getContents = async () => {
    let results = await filterPosts(query, page, cookies.session);
    setItems(results.posts);
  };

  const [items, setItems] = useState(() => {
    getContents(cookies.session);
  });

  return (
    <Container text>
      {items ? (
        <>
          {items.map(post => (
            <div key={post._id}>
              <Header as="h2" className="postTitle">
                {post.title}
                <p className="userPost">{post.user}</p>
              </Header>
              <Image
                src={post.image}
                className="imagePost"
                rounded
                size="huge"
                centered
              />
              <p className="postText">{post.post}</p>
              <Divider />
            </div>
          ))}
        </>
      ) : (
        "No records"
      )}
    </Container>
  );
};

const Post = props => {
  const [values, setValues] = useState({
    title: "",
    post: "",
    createdAt: "",
    user: ""
  });
  const [checkVal, setCheckVal] = useState(false);
  const [cookies, setCookie, removeCookie] = useCookies(["session"]);
  const [image, setImage] = useState();
  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };
  const getFile = event => {
    setImage(event[0]);
  };
  const checkValues = (title, post) => {
    if (!title || !post) {
      setCheckVal(true);
    } else {
      let date = new Date();
      createPost(title, image, post, date, cookies.session);
    }
  };
  return (
    <Container>
      <Grid
        textAlign="center"
        style={{ height: "100vh" }}
        verticalAlign="middle"
      >
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as="h2" color="blue" textAlign="center">
            {" "}
            New Post
          </Header>
          <Form size="large">
            <Segment stacked>
              <Form.Input
                fluid
                icon="book"
                iconPosition="left"
                value={values.title}
                onChange={handleChange("title")}
                placeholder="Title"
              />
              <Form.Input
                fluid
                icon="book"
                iconPosition="left"
                value={values.image}
                onChange={e => getFile(e.target.files)}
                placeholder="Image"
                type="file"
              />
              <TextArea
                placeholder="Tell us more"
                value={values.post}
                onChange={handleChange("post")}
              />
              <Button
                color="blue"
                onClick={() => checkValues(values.title, values.post)}
                fluid
                size="large"
              >
                Register
              </Button>
            </Segment>
            {checkVal ? <p>Please write a valid form.</p> : ""}
          </Form>
        </Grid.Column>
      </Grid>
    </Container>
  );
};

export { Post, PostFiltered };
