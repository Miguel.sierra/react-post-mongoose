/* eslint-disable max-len */

import React, { useState, useEffect } from "react";
import {
  Container,
  Header,
  Image,
  Divider,
  Button,
  Icon
} from "semantic-ui-react";
import { myPosts, deletePost } from "../user/controllers/post";
import { useCookies, Cookies } from "react-cookie";

const MyPost = () => {
  const [cookies, setCookie, removeCookie] = useCookies(["session"]);

  const getContents = async () => {
    let results = await myPosts(cookies.session);
    setItems(results.posts);
  };

  const [items, setItems] = useState(() => {
    getContents(cookies.session);
  });

  const deleteItem = async post => {
    let results = await deletePost(post._id, cookies.session);
    if (results) {
      const filteredItems = items.filter(item => item._id !== post._id);
      setItems(filteredItems);
    }
  };

  return (
    <Container text>
      {items ? (
        <>
          {items.map(post => (
            <div key={post._id}>
              <Header as="h2" className="postTitle">
                {post.title}
                <p className="userPost">{post.user}</p>
              </Header>
              <Button negative floated="right" onClick={() => deleteItem(post)}>
                <Icon name="delete" />
              </Button>
              <Image
                src={post.image}
                className="imagePost"
                rounded
                size="huge"
                centered
              />
              <p className="postText">{post.post}</p>
              <Divider />
            </div>
          ))}
        </>
      ) : (
        "No records"
      )}
    </Container>
  );
};

export default MyPost;
