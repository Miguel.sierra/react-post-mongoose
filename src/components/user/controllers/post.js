import axios from "axios";
import Swal from "sweetalert2";

async function myPosts(token) {
  try {
    let res = await axios({
      method: "GET",
      url: `${process.env.REACT_APP_BASE_URL_API}/post/`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    let { data } = res;
    return data;
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Wrong data, please check.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

async function deletePost(id, token) {
  try {
    let res = await axios({
      method: "DELETE",
      url: `${process.env.REACT_APP_BASE_URL_API}/post/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    let { data } = res;
    return data;
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Wrong data, please check.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

export { myPosts, deletePost };
