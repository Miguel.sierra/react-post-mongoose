import axios from "axios";
import Swal from "sweetalert2";

async function createUser(email, password, firstName, lastName, username) {
  try {
    let res = await axios({
      method: "POST",
      url: `${process.env.REACT_APP_BASE_URL_API}/user/register`,
      data: email,
      password,
      firstName,
      lastName,
      username
    });
    let { data } = res;
    return data.token;
  } catch (error) {
    Swal.fire({
      title: "Error!",
      text: "Wrong data, please check.",
      icon: "error",
      confirmButtonText: "Ok"
    });
    return false;
  }
}

export { createUser };
