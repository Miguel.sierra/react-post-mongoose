import React, { useState } from "react";

import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from "semantic-ui-react";
import { createSession } from "../auth/auth";
import { Redirect, Link } from "react-router-dom";
import { useCookies, Cookies } from "react-cookie";

const LoginForm = () => {
  const [values, setValues] = useState({
    email: "",
    password: ""
  });
  //   const [start, setStart] = useState(() => redirectIfLogged());
  const [checkVal, setCheckVal] = useState(false);
  const [cookies, setCookie, removeCookie] = useCookies(["session"]);

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };

  async function checkValues(email, password) {
    if (email && password) {
      let session = await createSession(email, password);
      console.log(session);
      if (session) {
        setCookie("session", session, { path: "/" });
        window.location.replace("/home");
      }
    } else {
      setCheckVal(true);
    }
  }

  return (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="blue" textAlign="center">
          {" "}
          Log-in to your account
        </Header>
        <Form size="large">
          <Segment stacked>
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              value={values.email}
              onChange={handleChange("email")}
              placeholder="E-mail address"
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              onChange={handleChange("password")}
              placeholder="Password"
              type="password"
            />

            <Button
              color="blue"
              onClick={() => checkValues(values.email, values.password)}
              fluid
              size="large"
            >
              Login
            </Button>
          </Segment>
          {checkVal ? <p>Please write a valid email and password.</p> : ""}
        </Form>
        <Link to="/register">
          {" "}
          <Message>New to us? Sign Upw</Message>
        </Link>
      </Grid.Column>
    </Grid>
  );
};

export default LoginForm;
