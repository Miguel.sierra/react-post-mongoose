import React, { useState } from "react";
import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from "semantic-ui-react";
import { Redirect, Link } from "react-router-dom";
import { useCookies, Cookies } from "react-cookie";
import { createUser } from "../user/controllers/user";

const LoginForm = () => {
  const [values, setValues] = useState({
    email: "",
    password: "",
    username: "",
    firstName: "",
    lastName: ""
  });
  //   const [start, setStart] = useState(() => redirectIfLogged());
  const [checkVal, setCheckVal] = useState(false);
  const [cookies, setCookie, removeCookie] = useCookies(["session"]);
  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value });
  };
  const checkValues = (firstName, email, username, password) => {
    if (!firstName || !email || !username || !password) {
      setCheckVal(true);
    } else {
      let token = createUser(values);
      if (token) {
        setCookie("session", token, { path: "/" });
      }
    }
  };

  return cookies.session ? (
    <Redirect to="/home" token={Cookies} />
  ) : (
    <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="blue" textAlign="center">
          {" "}
          Register user
        </Header>
        <Form size="large">
          <Segment stacked>
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              value={values.firstName}
              onChange={handleChange("firstName")}
              placeholder="Firstname"
            />
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              value={values.lastName}
              onChange={handleChange("lastName")}
              placeholder="Lastname"
            />
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              value={values.username}
              onChange={handleChange("username")}
              placeholder="Username"
            />
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              value={values.email}
              onChange={handleChange("email")}
              placeholder="E-mail address"
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              onChange={handleChange("password")}
              placeholder="Password"
              type="password"
            />

            <Button
              color="blue"
              onClick={() =>
                checkValues(
                  values.email,
                  values.password,
                  values.username,
                  values.firstName
                )
              }
              fluid
              size="large"
            >
              Register
            </Button>
          </Segment>
          {checkVal ? <p>Please write a valid form.</p> : ""}
        </Form>
        <Link to="/">
          <Message>Login here</Message>
        </Link>
      </Grid.Column>
    </Grid>
  );
};

export default LoginForm;
