import React, { useState } from "react";
import { Input, Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { useCookies } from "react-cookie";
import { destroySession } from "../auth/auth";

function MenuNav(props) {
  const [activeItem, setActiveItem] = useState({ activeItem: "home" });
  let handleItemClick = (e, { name }) => setActiveItem({ activeItem: name });
  const [cookies, setCookie, removeCookie] = useCookies(props.token);
  const logout = e => {
    let session = destroySession(cookies.session);
    if (session) {
      removeCookie("session");
      window.location.replace("/");
    }
  };
  const search = e => {
    if (e.key === "Enter") {
      window.location.replace(`/filter-posts/?page=0&query=${e.target.value}`);
    }
  };
  return (
    <Menu fixed="top" inverted>
      {cookies.session ? (
        <>
          <Link to="/home">
            <Menu.Item
              name="home"
              active={activeItem === "home"}
              onClick={handleItemClick}
            />
          </Link>
          <Link to="/new-post">
            <Menu.Item
              name="New post"
              active={activeItem === "New post"}
              onClick={handleItemClick}
            />
          </Link>
          <Menu.Menu position="right">
            <Menu.Item>
              <Input
                icon="search"
                onKeyDown={search}
                placeholder="Search Posts..."
              />
            </Menu.Item>
            <Menu.Item
              name="logout"
              active={activeItem === "logout"}
              onClick={logout}
            />
          </Menu.Menu>
        </>
      ) : (
        <Menu.Menu position="right">
          <Link to="/">
            <Menu.Item name="login" active={activeItem === "login"} />
          </Link>
        </Menu.Menu>
      )}
    </Menu>
  );
}

export default MenuNav;
