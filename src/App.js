import React, { useState, useEffect } from "react";
import axios from "axios";
import NotFound from "./components/layouts/404";
import MenuAppBar from "./components/layouts/nav";
import "./sass/App.scss";
import "semantic-ui-css/semantic.min.css";
import Loading from "./components/layouts/loader";
import Login from "./components/user/login";
import Register from "./components/user/register";
import { useCookies } from "react-cookie";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { Post, PostFiltered } from "./components/posts/post";
import MyPost from "./components/user/post";

function App() {
  const [cookies, setCookie, removeCookie] = useCookies(["session"]);

  const [session, setSession] = useState();

  const PrivateRoute = ({ component: Component, path, ...rest }) => {
    useEffect(() => {
      if (cookies.session) {
        axios({
          headers: { Authorization: `Bearer ${cookies.session}` },
          method: "GET",
          url: `${process.env.REACT_APP_BASE_URL_API}/user/info`
        })
          .then(element => {
            setSession(true);
          })
          .catch(e => {
            setSession(false);
          });
      } else {
        setSession(false);
      }
    }, [path]);

    const render = props =>
      session === true ? <Component {...props} /> : null;
    return <Route path={path} render={render} {...rest} />;
  };

  return (
    <>
      <MenuAppBar />
      <Switch>
        <Route exact path="/" component={() => <Login />}></Route>
        <Route exact path="/register" component={Register}></Route>
        <PrivateRoute exact path="/new-post" component={() => <Post />} />
        <PrivateRoute
          exact
          path="/filter-posts"
          component={() => <PostFiltered />}
        />
        <PrivateRoute exact path="/home" component={() => <MyPost />} />
        <Route path="*" component={NotFound} />
      </Switch>
    </>
  );
}

export default App;
